#!/bin/bash

DOWNLOAD_URL="https://developer.download.nvidia.com/hpc-sdk/22.11/nvhpc_2022_2211_Linux_x86_64_cuda_11.8.tar.gz"

mkdir -p /nvhpc
pushd /nvhpc

curl -L -O -J $DOWNLOAD_URL  
tar -xf nvhpc_2022_2211_Linux_x86_64_cuda_11.8.tar.gz
pushd nvhpc_2022_2211_Linux_x86_64_cuda_11.8
export NVHPC_SILENT=true 
export NVHPC_INSTALL_DIR="$pkgdir/opt/nvidia/hpc_sdk"
bash ./install
popd
source /nvhpc.sh
makelocalrc  -x /opt/nvidia/hpc_sdk/Linux_x86_64/22.11/compilers/bin
