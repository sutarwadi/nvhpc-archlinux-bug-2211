# ------------------------------------------------------------------------------
# SPDX-License-Identifier: "Apache-2.0"
# Copyright (C) 2021-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
# ------------------------------------------------------------------------------
FROM archlinux:latest
RUN pacman -Syu --noconfirm
RUN pacman -S --noconfirm gcc gcc-fortran
COPY install.sh /install.sh
COPY nvhpc.sh /nvhpc.sh
RUN bash install.sh
RUN mkdir -p source
COPY buggy.f90 /bug/
RUN useradd -m tester
RUN chown -vR tester:tester /bug
USER tester
