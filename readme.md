# Steps to Reproduce the Bug

## Build the Docker Container
```
docker build -t nvhpc-archbug .
```

## Reproduce the Example

Run the Docker Container
```
docker run -it nvhpc-archbug
```

Inside the container, run

```
bash reproduce.sh
```

You should get the following output
```
[tester@8827cc1fa07e bug]$ bash reproduce.sh
/bug /
--------------------------------------------------------------------------
The value of the MCA parameter "plm_rsh_agent" was set to a path
that could not be found:

  plm_rsh_agent: ssh : rsh

Please either unset the parameter, or check that the path is correct
--------------------------------------------------------------------------
[d7caacad7fe4:00025] [[INVALID],INVALID] FORCE-TERMINATE AT Not found:-13 - error ../../../../../orte/mca/plm/rsh/plm_rsh_component.c(327)
[d7caacad7fe4:00025] *** Process received signal ***
[d7caacad7fe4:00025] Signal: Segmentation fault (11)
[d7caacad7fe4:00025] Signal code: Address not mapped (1)
[d7caacad7fe4:00025] Failing at address: (nil)
[d7caacad7fe4:00025] [ 0] /usr/lib/libc.so.6(+0x38a00)[0x7f3183451a00]
[d7caacad7fe4:00025] *** End of error message ***
/opt/nvidia/hpc_sdk/Linux_x86_64/22.11/comm_libs/mpi/bin/mpirun: line 15:    25 Segmentation fault      (core dumped) $MY_DIR/.bin/$EXE "$@"
/
```
